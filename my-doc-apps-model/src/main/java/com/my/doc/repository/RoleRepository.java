package com.my.doc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.doc.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	public Role findByroleName(String roleName);
}
