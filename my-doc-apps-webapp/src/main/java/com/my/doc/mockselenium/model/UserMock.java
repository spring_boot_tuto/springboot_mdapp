package com.my.doc.mockselenium.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "auth_user")
public class UserMock{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "auth_user_id")
	private int id;

	@Column(name = "first_name")
	String firstName ;
	@Column(name = "Last_name")
	String lastName;
	@Column(name = "password")
	String password;
	@Transient
	String confirmPassword;
	@Column(name = "email")
	String email;
	@Transient
	String confirmEmail;
	@Column(name = "phone")
	String phone;
	@Transient
	String status;
	
	
	public UserMock(String lastName, String password, String email, String phone) {
		super();
		this.lastName = lastName;
		this.password = password;
		this.email = email;
		this.phone = phone;
	}
	public UserMock() {
		super();
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public int getId() {
		return id;
	}
	public String getEmail() {
		return email;
	}
	public String getConfirmEmail() {
		return confirmEmail;
	}
	public String getPhone() {
		return phone;
	}
	public String getStatus() {
		return status;
	}
	
	
}
