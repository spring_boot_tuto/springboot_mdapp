package com.my.doc.application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = { "com.my.doc.repository" ,"com.my.doc.mockrepository"})
@EntityScan(basePackages = { "com.my.doc.model", "com.my.doc.mockselenium.model"})
@ComponentScan(basePackages = { "com.my.doc.controller", "com.my.doc.service", "com.my.doc.repository",
		"com.my.doc.validator", "com.my.doc.userMock","com.my.doc.custom"})
public class WebApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class);
	}

}
