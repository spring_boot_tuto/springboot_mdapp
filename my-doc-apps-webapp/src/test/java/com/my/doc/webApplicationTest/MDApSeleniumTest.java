package com.my.doc.webApplicationTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import io.github.bonigarcia.wdm.WebDriverManager;

@ExtendWith(SpringExtension.class)
@Testcontainers
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MDApSeleniumTest {

	@SuppressWarnings("rawtypes")
	@Container
	public BrowserWebDriverContainer firefox = new BrowserWebDriverContainer().withCapabilities(new FirefoxOptions());

	RemoteWebDriver driver;
	@LocalServerPort
	private Integer port = 8080;
	private String baseUrl;

	@Before
	public void setUpWebDriver() {
		driver = firefox.getWebDriver();
		baseUrl = "http://localhost:" + port + "/";
	}

	@AfterEach
	public void quitWebDriver() {
		if (driver != null) {
			driver.quit();
		}
	}

	// Selenium Test
	@Test
	@DisplayName("The testproject.io web site should have the correct title")
	public void getSearchPage() {

		try {
			driver = firefox.getWebDriver();

			driver.get("http://" + firefox.getTestHostIpAddress() + ":8080/");

			driver.manage().window().maximize();

			driver.findElement(By.xpath("//*[@id=\"login\"]")).click();

			driver.findElement(By.xpath("//*[@id=\"firstName\"]")).sendKeys("Jean pierre");

			driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("poiuytreza0987654321");

			driver.findElement(By.xpath("//*[@id=\"login-submit\"]")).click();

			driver.findElement(By.xpath("//*[@id=\"logout\"]")).click();

			WebElement webE = driver.findElement(By.xpath("//*[@id=\"firstName\"]"));
			WebElement webE2 = driver.findElement(By.name("firstName"));
			assertThat(webE).isEqualTo(webE2);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
