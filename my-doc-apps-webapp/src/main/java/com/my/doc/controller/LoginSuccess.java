package com.my.doc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginSuccess {
	 @GetMapping({"/loginsuccess"})
	    public String loginSuccess(Model model) {
	        return "loginsuccess";
	    }

}
