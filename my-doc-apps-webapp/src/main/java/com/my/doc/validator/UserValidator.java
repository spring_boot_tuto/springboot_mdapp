package com.my.doc.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.my.doc.model.User;
import com.my.doc.service.UserService;

@Component
public class UserValidator implements Validator {
	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> aClass) {
		return User.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		User user = (User) o;

		String notEmpty = "NotEmpty";
		String fN = "firstName";
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", notEmpty);
		if (user.getFirstName().length() < 6 || user.getFirstName().length() > 32) {
			errors.rejectValue(fN, "Size.userForm.firstName");
		}
		if (userService.findByfirstName(user.getFirstName()) != null) {
			errors.rejectValue(fN, "Duplicate.userForm.firstName");
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", notEmpty);
		if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
			errors.rejectValue("password", "Size.userForm.password");
		}

		if (!user.getConfirmPassword().equals(user.getPassword())) {
			errors.rejectValue("confirmPassword", "Diff.userForm.confirmPassword");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", notEmpty);
		if (user.getPhone().length() < 10 || user.getPhone().length() > 10) {
			errors.rejectValue("phone", "Size.userForm.phone");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", notEmpty);
		if ( user.getEmail().isEmpty()) {
			errors.rejectValue("email", "NotNull.userForm.email");
		}
		if (!user.getConfirmEmail().equals(user.getEmail())) {
			errors.rejectValue("confirmEmail", "Diff.userForm.confirmEmail");
		}
	}
}
