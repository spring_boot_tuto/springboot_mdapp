package com.my.doc.service;

public interface SecurityService {
    String findLoggedInfirstName();

    void autoLogin(String firstName, String password);
}
