package com.my.doc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.my.doc.model.User;
import com.my.doc.service.SecurityService;
import com.my.doc.service.UserService;
import com.my.doc.validator.UserValidator;

@Controller
public class SigninController {
	@Autowired
	private UserService userService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserValidator userValidator;

	@GetMapping({ "/", "/signinpage" })
	public String registration(Model model) {
		model.addAttribute("user", new User());

		return "register";
	}

	@PostMapping("/signinpage")
	public String registration(@ModelAttribute("user") User user, BindingResult bindingResult) {
		userValidator.validate(user, bindingResult);

		if (bindingResult.hasErrors()) {
			return "register";
		}

		userService.save(user);

		securityService.autoLogin(user.getFirstName(), user.getConfirmPassword());

		return "redirect:/welcome";
	}
}
