package com.my.doc.service;

import com.my.doc.model.User;

public interface UserService {
	void save(User user);
	User findByfirstName(String firstName);
}
