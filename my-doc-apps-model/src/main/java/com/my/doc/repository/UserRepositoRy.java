package com.my.doc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.doc.model.User;

@Repository
public interface UserRepositoRy extends JpaRepository<User, Long> {
	 User findByFirstName(String firstName);
}
