package com.my.doc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {

	ModelAndView modelAndView = new ModelAndView();

	@GetMapping("/adminpage")
	public ModelAndView admin() {

		modelAndView.setViewName("admin"); // resources/template/admin.html
		return modelAndView;
	}

}
