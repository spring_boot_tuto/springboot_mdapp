package com.my.doc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.my.doc.mockrepository.UserRepositoryMock;
import com.my.doc.mockselenium.model.UserMock;

@Controller
public class UserMockController {
	
	//MockHttpServletResponse
		@Autowired
	    private UserRepositoryMock userRepository;
		@GetMapping(value="/admin/users")
		public ResponseEntity<List<UserMock>>users(){
		    List<UserMock> usersMDApp=userRepository.findAll();
		    if(usersMDApp.isEmpty())
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		    return  new ResponseEntity<>(usersMDApp,HttpStatus.OK);
		}

}
