package com.my.doc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	ModelAndView modelAndView = new ModelAndView();
	@GetMapping("/homepage")
	public ModelAndView home() {
		modelAndView.setViewName("home"); // resources/template/home.html
		return modelAndView;
	}

}
