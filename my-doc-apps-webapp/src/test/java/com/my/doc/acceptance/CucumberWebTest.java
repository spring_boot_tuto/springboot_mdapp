package com.my.doc.acceptance;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.my.doc.controller.UserMockController;
import com.my.doc.mockrepository.UserRepositoryMock;
import com.my.doc.mockselenium.model.UserMock;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

@SpringBootTest
@AutoConfigureMockMvc
public class CucumberWebTest {

	@InjectMocks
	UserMock userMock;

	@InjectMocks
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@InjectMocks
	private UserMockController controller;

	@Mock
	private UserRepositoryMock userRepositoryMock;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		// Initialization of mockito fields
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

	}

	@Given("un user utilise le MDAp")
	public void a_user_access__the_app() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/admin/users"))
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
	}

	@Test
	@DisplayName("Simulition de la création d'un user")
	@When("Création d'un user")
	public void save() throws Exception {
		userMock.getId();
		userMock.setEmail("mockmvc@mockmvc.moc");
		userMock.setConfirmEmail("mockmvc@mockmvc.moc");
		userMock.setFirstName("mockFirstName");
		userMock.setLastName("mockLastName");
		userMock.setPassword("12345678");
		userMock.setConfirmPassword("12345678");
		userMock.setPassword(bCryptPasswordEncoder.encode(userMock.getPassword()));
		userMock.setPhone("0625961534");
		userMock.setStatus("MOCKITO_SUCCESS");

		when(userRepositoryMock.findAll()).thenReturn(Collections.singletonList(userMock));

		assertThat(userMock.getFirstName()).isEqualTo("mockFirstName");
		mockMvc.perform(get("/admin/users")).andDo(print());

		userRepositoryMock.save(userMock);

	}

}