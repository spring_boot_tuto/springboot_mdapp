package com.my.doc.application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication

@EntityScan("com.my.doc.model")
@EnableJpaRepositories(basePackages = {"com.my.doc.repository"})
@ComponentScan(basePackages = {"com.my.doc.model","com.my.doc.service"})
public class MyDocAppsModelApplication extends  SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MyDocAppsModelApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(MyDocAppsModelApplication.class);
	}

}
