package com.my.doc.mockrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.my.doc.mockselenium.model.UserMock;

@Repository
public interface UserRepositoryMock extends JpaRepository<UserMock, Long> {
	UserMock findByFirstName(String firstName);
}
