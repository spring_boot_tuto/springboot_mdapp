package com.my.doc.service;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.my.doc.model.Role;
import com.my.doc.model.User;
import com.my.doc.repository.RoleRepository;
import com.my.doc.repository.UserRepositoRy;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
    private UserRepositoRy userRepository;
    @Autowired
    private RoleRepository roleRepositoryDebug;

    @Override
    public void save(User user) {
		
        user.setPassword(bCryptPasswordEncoder().encode(user.getPassword()));
        user.setStatus("VERIFIED");
        user.setRoles(new HashSet<>(roleRepositoryDebug.findAll()));
        user.setRoles(Arrays.asList(new Role("ADMIN_USER")));
        

        userRepository.save(user);
       
    }

    @Override
    public User findByfirstName(String firstName) {
        return userRepository.findByFirstName(firstName);
    }
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
